package Main;

import ATS.Block;
import ATS.Conditions.Condition;
import ATS.Const;
import ATS.Expressions.Expression;
import ATS.Procedure;
import ATS.Statements.*;
import ATS.Var;
import Tokens.Tokens;
import javafx.util.Pair;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Stack;


public class Parser {
    Stack<Pair<String, Tokens.token>> stack = new Stack<>();
    public Block parse() throws Exception {
        return readBlock();
    }

    private Block readBlock() throws Exception {
        Block block = new Block();
        if(stack.peek().getValue() == Tokens.token.CONST)
            block.addConsts(readConsts());

        if(stack.peek().getValue() == Tokens.token.VAR)
            block.addVars(readVars());

        while(stack.peek().getValue() == Tokens.token.PROCEDURE) {
            block.addProcedures(readProcedure());
        }

        block.setStatement(readStatement());
        return block;
    }

    private Statement readStatement() throws Exception {
        switch (stack.peek().getValue()){
            case IDENT: return readSetStatement();
            case CALL: return readCallStatement();
            case QMARK: return readReadStatement(); //READ
            case EMARK: return readWriteStatement(); //WRITE
            case BEGIN: return readBeginStatement();
            case IF: return readIfStatement();
            case WHILE: return readWhileStatement();
            default:
                throw new Exception("Invalid statement token");
        }
    }

    private Statement readWhileStatement() throws Exception {
        if(stack.pop().getValue() != Tokens.token.WHILE)
            throw new Exception("Expected WHILE");

        WhileStatement whileStatement = new WhileStatement();
        whileStatement.setCondition(readCondition());

        if(stack.pop().getValue() != Tokens.token.DO)
            throw new Exception("Expected DO");

        whileStatement.setStatement(readStatement());

        return whileStatement;
    }

    private Statement readIfStatement() throws Exception {
        if(stack.pop().getValue() != Tokens.token.IF)
            throw new Exception("Expected IF");

        IfStatement ifStatement = new IfStatement();
        ifStatement.setCondition(readCondition());

        if(stack.pop().getValue() != Tokens.token.THEN)
            throw new Exception("Expected THEN");

        ifStatement.setStatement(readStatement());

        return ifStatement;
    }

    private Statement readBeginStatement() throws Exception {
        if(stack.pop().getValue() != Tokens.token.BEGIN)
            throw new Exception("Expected BEGIN");

        BeginEndStatement beginEndStatement = new BeginEndStatement();

        while (stack.peek().getValue() != Tokens.token.END){
            beginEndStatement.addStatement(readStatement());

            if(stack.peek().getValue() == Tokens.token.EOS || stack.peek().getValue() == Tokens.token.END){
                if(stack.peek().getValue() == Tokens.token.EOS) {
                    stack.pop();
                }
            } else {
                throw new Exception("Expected END or ;");
            }
        }

        stack.pop(); //Smaže "END"
        return beginEndStatement;
    }

    private Statement readWriteStatement() throws Exception {
        if(stack.pop().getValue() != Tokens.token.EMARK)
            throw new Exception("Expected !");

        if(stack.peek().getValue() != Tokens.token.COLONEQUALS)
            throw new Exception("Expected :=");

        return  new WriteStatement(readExpression());
    }

    private Statement readReadStatement() throws Exception {
        if(stack.pop().getValue() != Tokens.token.QMARK)
            throw new Exception("Expected ?");

        return new ReadStatement(stack.pop().getKey());
    }

    private Statement readCallStatement() throws Exception {
        if(stack.pop().getValue() != Tokens.token.IDENT)
            throw new Exception("Expected IDENT");

        return new CallStatement(stack.pop().getKey());
    }

    private Statement readSetStatement() throws Exception {
        if(stack.peek().getValue() != Tokens.token.IDENT)
            throw new Exception("Expected IDENT");

        SetStatement setStatement = new SetStatement();
        setStatement.setIdent(stack.pop().getKey());

        if(stack.peek().getValue() != Tokens.token.COLONEQUALS)
            throw new Exception("Expected :=");

        setStatement.setExpression(readExpression());
        return  setStatement;
    }

    private Expression readExpression() {
        throw new NotImplementedException();
    }


    //asi špatně?
    private Const readConsts() throws Exception {
        if(stack.peek().getValue() != Tokens.token.CONST){
            throw new Exception("Expected const");
        } else {
            stack.pop();
        }

        Const cons = new Const();
        cons.setIdent(stack.pop().getKey());

        if(stack.peek().getValue() != Tokens.token.EQUALS) {
            throw new Exception("Expected =");
        } else {
            stack.pop();
        }

        cons.setValue(stack.pop().getKey());

        if(stack.peek().getValue() != Tokens.token.EOS) {
            throw new Exception("Expected ;");
        } else {
            stack.pop();
        }

        return cons;
    }

    private Var readVars(){
        throw new NotImplementedException();
    }

    private Procedure readProcedure(){
        throw new NotImplementedException();
    }

    private Condition readCondition() {
        throw new NotImplementedException();
    }
}
