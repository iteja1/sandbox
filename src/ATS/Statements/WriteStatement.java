package ATS.Statements;

import ATS.Conditions.Condition;
import ATS.Expressions.Expression;

public class WriteStatement extends Statement {
    Expression expression;

    public WriteStatement(Expression expression) {
        this.expression = expression;
    }
}
