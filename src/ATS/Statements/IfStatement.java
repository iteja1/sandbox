package ATS.Statements;

import ATS.Conditions.Condition;

public class IfStatement extends Statement {
    Statement statement;
    Condition condition;

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }
}
