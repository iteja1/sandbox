package ATS.Statements;

import ATS.Expressions.Expression;

public class SetStatement extends Statement {
    String ident;
    Expression expression;

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
