package ATS;

import ATS.Statements.Statement;

import java.util.List;

public class Block {
    List<Const> Consts;
    List<Var> Vars;
    List<Procedure> Procedures;

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    Statement statement;

    public void addConsts(Const constant){
        Consts.add(constant);
    }

    public void addVars(Var var){
        Vars.add(var);
    }

    public void addProcedures(Procedure procedure){
        Procedures.add(procedure);
    }
}
