package ATS.Conditions;

import ATS.Expressions.Expression;

public class OddCondition extends Condition {
    Expression expression;

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
